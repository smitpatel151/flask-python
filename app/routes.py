from app import app
from flask import render_template

@app.route('/')
def index():
    print("main page")
    return render_template("index.html",title="WAPP")

@app.route('/somepage')
def somepage():
    return render_template("somepage.html",title="WAPP")